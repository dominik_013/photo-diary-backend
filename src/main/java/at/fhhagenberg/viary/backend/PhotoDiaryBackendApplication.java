package at.fhhagenberg.viary.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PhotoDiaryBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(PhotoDiaryBackendApplication.class, args);
	}
}
