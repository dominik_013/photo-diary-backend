package at.fhhagenberg.viary.backend.controller;


import at.fhhagenberg.viary.backend.annotations.ActiveUser;
import at.fhhagenberg.viary.backend.model.DiaryEntry;
import at.fhhagenberg.viary.backend.model.User;
import at.fhhagenberg.viary.backend.security.CustomUserDetails;
import at.fhhagenberg.viary.backend.service.DiaryEntryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.MediaType;

import java.util.Collection;


@RestController
public class DiaryEntryController {

    @Autowired
    private DiaryEntryService diaryEntryService;

    /**
     * Gets all diary entries for the current authenticated user
     *
     * @return The http response containing the entries in JSON and HttpStatus.OK if everything went right
     * If there was no user authenticated HttpStatusCode 401 is returned
     */
    @RequestMapping(value = "/api/diaryentries",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Collection<DiaryEntry>> getDiaryEntriesForUser() {
        Collection<DiaryEntry> diaryEntries = diaryEntryService.findAllForUser();

        return new ResponseEntity<>(diaryEntries, HttpStatus.OK);
    }

    /**
     * Creates a diary-entry for the current authenticated user
     *
     * @param entry The diary entry to be created
     * @param activeUser The current authorized user to which we should save the diary-entry
     * @return The http response with the created entry as content and HttpStatus.CREATED if everything went right
     */
    @RequestMapping(value = "/api/diaryentries",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DiaryEntry> createDiaryEntry(@RequestBody DiaryEntry entry, @ActiveUser User activeUser) {
        entry.setUserId(activeUser.getUserId());
        DiaryEntry savedEntry = diaryEntryService.create(entry);

        return new ResponseEntity<>(savedEntry, HttpStatus.CREATED);
    }
}