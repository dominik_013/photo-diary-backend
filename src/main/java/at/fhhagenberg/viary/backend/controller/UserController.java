package at.fhhagenberg.viary.backend.controller;


import at.fhhagenberg.viary.backend.annotations.ActiveUser;
import at.fhhagenberg.viary.backend.model.User;
import at.fhhagenberg.viary.backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * Tries to insert a user in the database
     *
     * If the email already exists an http-status 500 error is returned
     * @param user The user which has to be persisted in the database
     * @return A response containing the saved user in json and HttpStatus.CREATED if
     * everything went good
     */
    @RequestMapping(path = "/api/register",
    method = RequestMethod.POST,
    consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> createUser(@RequestBody User user) {
        User savedUser = userService.create(user);
        return new ResponseEntity<>(savedUser, HttpStatus.CREATED);
    }

    /**
     * Sets the user inactive so he can no longer perform any requests
     *
     * @param user The current authenticated user
     * @return The user which was unregistered
     */
    @RequestMapping(path = "/api/unregister",
    method = RequestMethod.POST)
    public ResponseEntity<User> setUserInactive(@ActiveUser User user) {
        User savedUser = userService.setInactive(user);
        SecurityContextHolder.getContext().setAuthentication(null);
        return new ResponseEntity<>(savedUser, HttpStatus.OK);
    }
}
