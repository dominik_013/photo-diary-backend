package at.fhhagenberg.viary.backend.model;


import org.joda.time.DateTime;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "diary_entries")
public class DiaryEntry {

    @Id
    @GeneratedValue
    private Long id;

    // The id to which user the entry belongs to
    private Long userId;

    private String title;

    private String note;

    private boolean isPrivate;

    private DateTime fromTime;

    private DateTime toTime;

    /**
     * A diary-entry contains more images
     * FetchType.Lazy = fetch when needed (when getter is called) FetchType.Eager fetch immediately
     */
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "diaryEntry")
    private Set<Image> images;

    private boolean isManualCompletionActive;

    private boolean isManualCompletionFinished;

    public DiaryEntry() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public DateTime getFrom() {
        return fromTime;
    }

    public void setFrom(DateTime from) {
        this.fromTime = from;
    }

    public DateTime getTo() {
        return toTime;
    }

    public void setTo(DateTime to) {
        this.toTime = to;
    }

    public Set<Image> getImages() {
        return images;
    }

    public void setImages(Set<Image> images) {
        this.images = images;
    }

    public boolean isManualCompletionActive() {
        return isManualCompletionActive;
    }

    public void setManualCompletionActive(boolean manualCompletionActive) {
        isManualCompletionActive = manualCompletionActive;
    }

    public boolean isManualCompletionFinished() {
        return isManualCompletionFinished;
    }

    public void setManualCompletionFinished(boolean manualCompletionFinished) {
        isManualCompletionFinished = manualCompletionFinished;
    }
}