package at.fhhagenberg.viary.backend.model;


import org.joda.time.DateTime;

import javax.persistence.*;

@Entity
@Table(name = "images")
public class Image {

    @Id
    @GeneratedValue
    private Long imageId;

    private String filePath;

    private DateTime timeTaken;

    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name="diary_entry_id")
    private DiaryEntry diaryEntry;

    public Image() {

    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public DateTime getTimeTaken() {
        return timeTaken;
    }

    public void setTimeTaken(DateTime timeTaken) {
        this.timeTaken = timeTaken;
    }

    public DiaryEntry getDiaryEntry() {
        return diaryEntry;
    }

    public void setDiaryEntry(DiaryEntry diaryEntry) {
        this.diaryEntry = diaryEntry;
    }
}
