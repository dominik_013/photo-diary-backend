package at.fhhagenberg.viary.backend.repository;


import at.fhhagenberg.viary.backend.model.DiaryEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;

public interface DiaryEntryRepository extends JpaRepository<DiaryEntry, Long> {

    /**
     * Runs a query which finds all diary entries for the current authenticated user
     *
     * @return A collection of the diary entries
     */
   @Query("select d from DiaryEntry d where d.userId = ?#{principal.userId}")
   Collection<DiaryEntry> findDiaryEntriesForUser();
}
