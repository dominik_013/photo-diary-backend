package at.fhhagenberg.viary.backend.repository;


import at.fhhagenberg.viary.backend.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    /**
     * Runs a query which finds a user by email [this is needed for the UserDetailsService and security]
     *
     * @param email The email from the user we want to find
     * @return The user associated with the passed email
     */
    public User findByEmail(String email);
}
