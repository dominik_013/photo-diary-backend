package at.fhhagenberg.viary.backend.service;

import at.fhhagenberg.viary.backend.model.DiaryEntry;

import java.util.Collection;

public interface DiaryEntryService {
    /**
     * Find all DiaryEntry entities.
     *
     * @return A Collection of DiaryEntry objects.
     */
    Collection<DiaryEntry> findAll();

    /**
     * Find all DiaryEntry entities which belong to the authorized user.
     *
     * @return A Collection of DiaryEntry objects belonging to the authorized user.
     */
    Collection<DiaryEntry> findAllForUser();

    /**
     * Persists a DiaryEntry entity in the data store.
     *
     * @param diaryEntry A User object to be persisted.
     * @return The persisted User entity.
     */
    DiaryEntry create(DiaryEntry diaryEntry);
}
