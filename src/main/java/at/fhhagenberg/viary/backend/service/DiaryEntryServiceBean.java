package at.fhhagenberg.viary.backend.service;

import at.fhhagenberg.viary.backend.model.DiaryEntry;
import at.fhhagenberg.viary.backend.repository.DiaryEntryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;
import java.util.Collection;

/**
 * The DiaryEntryServiceBean encapsulates all business behaviors operating on the
 * DiaryEntry entity model object.
 */
@Service
public class DiaryEntryServiceBean implements DiaryEntryService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private DiaryEntryRepository diaryEntryRepository;

    @Override
    public Collection<DiaryEntry> findAll() {
        Collection<DiaryEntry> diaryEntries = diaryEntryRepository.findAll();

        return diaryEntries;
    }

    @Override
    public Collection<DiaryEntry> findAllForUser() {
        Collection<DiaryEntry> diaryEntries = diaryEntryRepository.findDiaryEntriesForUser();
        //todo: https://www.youtube.com/watch?v=H94Wbd8ARKM   minute 49:00 add restriction in security??
        return diaryEntries;
    }

    @Override
    public DiaryEntry create(DiaryEntry diaryEntry) {
        logger.info("> create");

        // Ensure the entity object to be created does NOT exist in the
        // repository. Prevent the default behavior of save() which will update
        // an existing entity if the entity matching the supplied id exists.
        if (diaryEntry.getId() != null) {
            // Cannot create DiaryEntry with specified ID value
            logger.error("Attempted to create a DiaryEntry, but id attribute was not null.");
            throw new EntityExistsException("The id attribute must be null to persist a new entity.");
        }

        DiaryEntry savedEntry = diaryEntryRepository.save(diaryEntry);
        logger.info("< create");

        return savedEntry;
    }
}
