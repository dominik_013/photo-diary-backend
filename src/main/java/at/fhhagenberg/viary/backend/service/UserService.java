package at.fhhagenberg.viary.backend.service;

import at.fhhagenberg.viary.backend.model.User;

public interface UserService {
    /**
     * Persists a User entity in the data store.
     *
     * @param user A User object to be persisted.
     * @return The persisted User entity.
     */
    User create(User user);

    // TODO: update user to inactive

    User setInactive(User user);
}
