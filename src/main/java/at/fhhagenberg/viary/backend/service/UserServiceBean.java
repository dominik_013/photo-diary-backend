package at.fhhagenberg.viary.backend.service;

import at.fhhagenberg.viary.backend.model.User;
import at.fhhagenberg.viary.backend.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;

@Service
public class UserServiceBean implements UserService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public User create(User user) {
        logger.info("> create");

        // Ensure the entity object to be created does NOT exist in the
        // repository. Prevent the default behavior of save() which will update
        // an existing entity if the entity matching the supplied id exists.
        if (user.getUserId() != null) {
            // Cannot create User with specified ID value
            logger.error("Attempted to create a User, but id attribute was not null.");
            throw new EntityExistsException("The id attribute must be null to persist a new entity.");
        }

        user.setPassword(passwordEncoder.encode(user.getPassword()));

        User savedUser = userRepository.save(user);
        logger.info("< create");

        return savedUser;
    }

    @Override
    public User setInactive(User user) {
        User savedUser = userRepository.findByEmail(user.getEmail());
        savedUser.setActive(false);
        savedUser = userRepository.save(savedUser);
        return savedUser;
    }
}
